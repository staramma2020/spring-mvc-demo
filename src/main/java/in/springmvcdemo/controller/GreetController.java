package in.springmvcdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GreetController {

	@GetMapping("/greet")
	public ModelAndView greetMsg() {
	ModelAndView mav = new ModelAndView();
	mav.addObject("msg", "Hello, Good Evening..!!");
	mav.setViewName("dashboard");
	return mav;

}
}
